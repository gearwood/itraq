Element.prototype.initMinMaxRange = function(args){
    args = args || {};
    this.min = args.min || 0;
    this.max = args.max || 0;
    this.gridClass = 'grid_container';
    this.showGrid = args.showGrid || false;
    this.showScaleText = args.showScaleText || false;
    this.fixMin = args.fixMin || false;
    this.scaleBgColor = args.scaleBgColor || undefined;
    this.minLength = args.minLength || 1;
    this.slider = this.getElementsByClassName('slider')[0];
    this.pseudos = this.getElementsByClassName('pull_circle');
    this.minIndex = 0;
    this.maxIndex = 0;
    this.leftOffset = 0;
    this.isActive = false;
    var self = this;
    this.setRange = function(rangeObj){
        rangeObj = rangeObj || {};
        rangeObj.left = rangeObj.left ?  this.valueToScaleIndex(rangeObj.left) : 0;
        rangeObj.right = rangeObj.right ?  this.valueToScaleIndex(rangeObj.right) : this.scaleLength - 1;
        this.range = rangeObj;
    }
    this.eventsObj = {
        change : document.createEvent('Event')
    }
    this.eventsObj.change.initEvent('change', true, true);
    this.defaultScale = function(){
        var result = {};
        for(var i = 0; i <= 100; i++){
            result[i] = i;
        }
        return result;
    }

    this.scale = args.scale || this.defaultScale();
    if(this.showScaleText){
        this.classList.add('with_text_grid');
    }
    if(this.scaleBgColor){
        this.slider.style.backgroundColor = '#'+this.scaleBgColor;
    }
    this.drawGrid = function(){
        var container = document.createElement('div');
        container.classList.add(this.gridClass);
        this.appendChild(container);
        var mark_offset = 100/(this.scaleLength - 1);
        var i = 0;
        this.gridMarksArr = {};
        for(var key in this.scale){
            var new_mark = document.createElement('div');
            new_mark.style.left = mark_offset * i + '%';
            if(this.showScaleText){
                var text_mark = document.createElement('div');
                text_mark.classList.add('mark_text');
                text_mark.classList.add('bright');
                text_mark.classList.add('bolder');
                text_mark.innerHTML = this.scale[key];
                new_mark.appendChild(text_mark);
            }
            this.gridMarksArr[i] = new_mark;
            container.appendChild(new_mark);
            i++;
        }
    }
    this.pseudos[0].addEventListener('mousedown', function(){
        self.start('min');
    })
    this.pseudos[1].addEventListener('mousedown', function(e){
        self.start('max');
    })
    if(this.fixMin){
        this.pseudos[0].style.display = 'none';
    }
    this.convertScaleToArr = function(){
        var result = {};
        var i = 0;
        for(var key in this.scale){
            if(this.min == key) this.minIndex = i;
            if(this.max == key) this.maxIndex = i;
            result[i] = key;
            i++;
        }
        this.scaleArray = result;
    }
    this.valueToScaleIndex = function(value){
        for(var i = 0; i < this.scaleLength; i++){
            if(this.scaleArray[i] == value) return i;
        }
    }

    this.start = function(activeField){
        this.isActive = true;
        this.activeField = activeField;
        this.rect = this.getBoundingClientRect();
    }
    this.stop = function(e){
        this.isActive = false;
    }
    document.addEventListener('mouseup', function(){
        self.stop();
    })
    document.addEventListener('mousemove', function(e){
        if(!self.isActive) return;
        var rangeLeft = e.clientX - self.rect.left;
        var percent = rangeLeft/self.rect.width*100;
        percent = percent.toFixed(2);
        percent = percent > 100 ? 100 : percent;
        percent = percent < 0 ? 0 : percent;
        var changed = false;
        if (self.activeField == 'min'){
            changed = self.setMin(percent);
        }else{
            changed = self.setMax(percent);
        }
        if(changed){
            self.draw();
            self.dispatchEvent(self.eventsObj.change);
        }
        e.target.ownerDocument.defaultView.getSelection().removeAllRanges();
        return false;
    });

    this.percentToValueIndex = function(percent){
        var widthPerc = 100/(this.scaleLength - 1);
        var floorIndex = Math.floor(percent/widthPerc);
        var floorPerc = floorIndex*widthPerc;
        var index = (floorPerc+widthPerc/2) > percent ? floorIndex : floorIndex+1;
        return index;
    }
    this.indexToPercent = function(index){
        var widthPerc = 100/(this.scaleLength - 1);
        return widthPerc * index;
    }
    this.draw = function(){
        var minPerc = this.indexToPercent(this.minIndex);
        this.pseudos[0].style.left = minPerc + '%';
        this.pseudos[1].style.left = this.indexToPercent(this.maxIndex) + '%';
        this.slider.style.left = minPerc + '%';
        this.slider.style.width = this.indexToPercent(this.maxIndex - this.minIndex) + '%';
        window.setTimeout(function(){
            self.classList.add('ready');
        },1);
    }
    this.setMin = function(percent){
        var oldMin = +this.min;
        var index = self.percentToValueIndex(percent);
        if (index >= this.maxIndex - this.minLength) {
            index = this.maxIndex - this.minLength;
        }
        if(this.range.left > index){
            index = this.range.left;
        }
        index = this.fixMin ? 0 : index;
        var min = this.scaleArray[index];
        this.min = min;
        this.minIndex = index;
        return oldMin != this.min;
    }
    this.setMax = function(percent){
        var oldMax = +this.max;
        var index = self.percentToValueIndex(percent);
        if (index <= this.minIndex + this.minLength && !this.fixMin){
            index = this.minIndex + this.minLength;
        }
        if(this.range.right < index){
            index = this.range.right;
        }
        if(index < this.minIndex && this.fixMin){
            index = this.minIndex;
        }
        var max = this.scaleArray[index];
        this.max = max;
        this.maxIndex = index;
        return oldMax != this.max;
    }
    this.setColor = function(color){
        if(color)
            this.slider.style.backgroundColor = color;
    }
    this.scaleLength = Object.keys(this.scale).length;
    if(this.showGrid){
        this.drawGrid();
    }
    this.convertScaleToArr();
    this.setRange();
    this.setMin(this.indexToPercent(this.minIndex));
    this.draw();
    this.setColor(args.color);
}
