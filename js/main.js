/* BEGIN scrolling */
document.addEventListener("DOMContentLoaded", function(){

    function initScrollBar(items){
        for(var i = 0; i < items.length; i++){
            Ps.initialize(items[i]);
        }
    }

    function destroyScrollBar(items){
        for(var i = 0; i < items.length; i++){
            Ps.destroy(items[i]);
        }
    }
    function resize() {
        var elements = document.querySelectorAll('.scroll_block');
        destroyScrollBar(elements);
        initScrollBar(elements);
    }
    window.onresize = resize;
    window.onresize();
    /* END Scrolling */

    /* BEGIN slider */
    var slider = document.getElementById('gr_slider');
    slider.initSlider();

    /* END slider */


    /* BEGIN Calendar Usage  section */

    //initCalendar();

    /* END Calendar Usage  section */


})

var schedule,fixTimeSchedule;
function add_new_period() {
    schedule.addPeriod();
}
function add_new_moment() {
    fixTimeSchedule.addPeriod();
}

function serializeSchedule(sch){
    console.log(sch.export());
}
function initCustomShcedule() {
    /* show block*/
    document.getElementById('schedulePicker').classList.add('custom_schedule_mode');
    document.getElementById('schedulePicker').classList.remove('opened');
    if(schedule) return false;
    /* BEGIN Customer schedule usage */
    var time_line = document.getElementById('scheduleLineContainer');
    schedule = new Schedule();
    /* schedule vizualizer */
    var sch_visualizer = new ScheduleVisualizer(schedule);
    var addPeriodButtonDOM = document.getElementById('addPeriodButton');
    sch_visualizer.vizualizeTo(time_line);
    sch_visualizer.vizualizerDOM.addEventListener('maxReached',function(){
        addPeriodButtonDOM.disabled = true;
    });
    sch_visualizer.vizualizerDOM.addEventListener('maxNotReached',function(){
        addPeriodButtonDOM.disabled = false;
    });
    /*  BEGIN Init default periods */
    schedule.addPeriod({
        timeFrom : 8 * 60,
        timeTill : 14 * 60,
        frequency : 1 * 60,
    });
    schedule.addPeriod({
        timeFrom : 14 * 60,
        timeTill : 18 * 60,
        frequency : 20,
    });
    /*  END Init default periods */
    /* END Customer schedule usage */
}
function initFixTimeShcedule() {
    /* show block*/
    document.getElementById('schedulePicker').classList.add('fix_schedule_mode');
    document.getElementById('schedulePicker').classList.remove('opened');
    if(fixTimeSchedule) return false;

    /* BEGIN Customer schedule usage */
    var time_line = document.getElementById('fixTimeScheduleLineContainer');
    fixTimeSchedule = new Schedule({
        periodTemplateID : 'momentTemplate',
        periodsContainerID : 'momentsLineContainer',
        fixMode : true,
        maxPeriods : 10,
    });
    /* schedule vizualizer */
    var sch_visualizer = new ScheduleVisualizer(fixTimeSchedule);
    var addPeriodButtonDOM = document.getElementById('addFixMomentButton');
    sch_visualizer.vizualizeTo(time_line);
    sch_visualizer.vizualizerDOM.addEventListener('maxReached',function(){
        addPeriodButtonDOM.disabled = true;
    });
    sch_visualizer.vizualizerDOM.addEventListener('maxNotReached',function(){
        addPeriodButtonDOM.disabled = false;
    });
    /*  BEGIN Init default periods */
    fixTimeSchedule.addPeriod({
        timeFrom : 0,
        timeTill : 8 * 60,
        frequency : 10,
    });
    fixTimeSchedule.addPeriod({
        timeFrom : 0,
        timeTill : 20 * 60,
        frequency : 10,
    });
    /*  END Init default periods */
    /* END Customer schedule usage */

}


// BEGIN google maps */
var citymap = {
    chicago: {
        center: {lat: 41.878, lng: -87.629},
        population: 2714856
    },
    newyork: {
        center: {lat: 40.714, lng: -74.005},
        population: 8405837
    }
};
var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 41.296, lng: -80.817},
        zoom: 6,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        rotateControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
    });
    for (var city in citymap) {
        // Add the circle for this city to the map.
        var cityCircle = new google.maps.Circle({
            strokeColor: '#2d4b64',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#6e93b1',
            fillOpacity: 0.35,
            map: map,
            center: citymap[city].center,
            radius: Math.sqrt(citymap[city].population) * 100
        });
    }
}

/* END google maps */


/* BEGIN Calendar Init section */
var currentDate = new Date();
function initCalendar(){
    document.getElementById('track_historyID').classList.add('choose_date_mode');
    var domCalendar = document.getElementById('calendarID');
    var calendar = new Calendar({
        date : currentDate,
        DOMObject : domCalendar
    });
    domCalendar.addEventListener('change',function(){
        currentDate = calendar.value;
        var new_value =  calendar.getDate();
        document.getElementById('track_historyID').classList.remove('choose_date_mode');
        document.getElementById('sch_date_containerID').innerText = new_value;
    });

}

/* END Calendar Init section */