
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
String.prototype.toDOM=function(){
    var wrapper = document.createElement('div');
    wrapper.innerHTML = this;
    return wrapper.firstChild;
};
function pad(num, size) {
    var s = '0' + num;
    return s.substr(s.length-size);
}

function getMaxOfArray(numArray) {
    return Math.max.apply(null, numArray);
}
function getMinOfArray(numArray) {
    return Math.min.apply(null, numArray);
}