function Calendar(args){
    args = args || {};
    if(!args.DOMObject){throw new Error('DOM Object required')}
    this.DOMObject = args.DOMObject;
    this.rowTemplate = args.rowTemplate || '<div class="row iTrq_line bolder cal_line"></div>';
    this.cellTemplate = args.cellTemplate || '<div class="col-xs-1"><div class="date_cell"></div></div>';
    this.activeCellClass = args.activeCellClass || 'active';
    this.activeLineClass = args.activeLineClass || 'active';
    this.todayCellClass = args.todayCellClass || 'today';
    this.anotherMonthClass = args.anotherMonthClass || 'anotherMonth';
    this.value = args.date || undefined;
    this.focus = args.date || undefined;
    this.dates = [];

    this.changeEvent = document.createEvent('Event');
    this.changeEvent.initEvent('change', true, true);

    this.init = function(){
        if(!this.value){
            this.value = new Date();
            this.focus = this.value;
        }
        this.findDOMElements();
        var self = this;
        this.DOMElements.pervMonth.addEventListener('click', function(){
            self.prevPage();
        });
        this.DOMElements.nextMonth.addEventListener('click', function(){
            self.nextPage();
        });
        this.draw();
    }

    this.setDate = function(date){
        var changed = date != this.value;
        this.value = date;
        this.focus = date;
        if(changed){
            this.onChange();
        }
    }
    this.getDate = function(){
        var monthStr = this.monthLabel(this.value.getMonth()).substring(0,3);
        var yearStr = this.value.getFullYear().toString().substring(2, 4);
        return this.value.getDate() + ' ' + monthStr + ', ' + yearStr;
    }
    this.onChange = function(){
        this.draw();
        this.DOMObject.dispatchEvent(this.changeEvent);
    }

    this.monthLabel = function(month){
        var labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        return labels[month];
    }
    this.DOMElementsClasses = [
        'monthContainer',
        'pervMonth',
        'nextMonth',
        'daysContainer'
    ];
    this.DOMElements = {};
    this.findDOMElements = function(){
        for(var i = 0; i < this.DOMElementsClasses.length; i++){
            var label = this.DOMElementsClasses[i];
            this.DOMElements[label] = this.DOMObject.getElementsByClassName(label)[0];
        }
    }
    this.curMonth = function(date){
        return new Date(date.getFullYear(), date.getMonth(), 1);
    }
    this.nextMonth = function(date){
        var nextMonth = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        nextMonth.setMonth(date.getMonth() + 1);
        return nextMonth;
    }
    this.prevMonth = function(date){
        var prevMonth = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        prevMonth.setMonth(date.getMonth() - 1);
        return prevMonth;
    }
    this.fillDates = function(){
        this.dates = [];
        var curMonth = this.curMonth(this.focus);
        var prevMonth = this.prevMonth(curMonth);
        var nextMonth = this.nextMonth(curMonth);

        var daysInCurrentMonth = curMonth.getDaysInMonth();
        var daysInPrevMonth = prevMonth.getDaysInMonth();
        var lastDay = new Date(curMonth.getFullYear(), curMonth.getMonth(), daysInCurrentMonth);
        var firstDayOfWeek = curMonth.getDay();
        var lastDayOfWeek = lastDay.getDay();
        /* first line */
        if (firstDayOfWeek > 0){
            var offset = daysInPrevMonth - (firstDayOfWeek - 1);
            for(var i = 0; i < firstDayOfWeek; i++){
                var date = offset + i;
                this.dates.push({
                    date : date,
                    month : prevMonth.getMonth(),
                    year : prevMonth.getFullYear(),
                });
            }
        }
        /* days of current month */
        for (var i = 1; i <= daysInCurrentMonth; i++){
            this.dates.push({
                date : i,
                month : curMonth.getMonth(),
                year : curMonth.getFullYear(),
            });
        }

        /* last line */
        if(lastDayOfWeek < 6){
            var backOffset = 6 - lastDayOfWeek;
            for (var i = 1; i <= backOffset; i++){
                this.dates.push({
                    date : i,
                    month : nextMonth.getMonth(),
                    year : nextMonth.getFullYear(),
                });
            }
        }
    }
    this.setFocus = function(date){
        this.focus = date;
        this.draw();
    }

    this.nextPage = function(){
        var focusCurMonth = this.curMonth(this.focus);
        var focusNextMonth = this.nextMonth(focusCurMonth);
        this.setFocus(focusNextMonth);
    }
    this.prevPage = function(){
        var focusCurMonth = this.curMonth(this.focus);
        var focusPrevMonth = this.prevMonth(focusCurMonth);
        this.setFocus(focusPrevMonth);
    }

    this.draw = function(){
        this.fillDates();
        var curDate = this.value.getDate();
        var curMonth = this.value.getMonth();
        var curYear = this.value.getFullYear();
        var curFocusDate = this.focus.getDate();
        var curFocusMonth = this.focus.getMonth();
        var curFocusYear = this.focus.getFullYear();
        var today = new Date();
        this.DOMElements.daysContainer.innerHTML = '';
        var line;
        for(var i = 0; i < this.dates.length; i++){
            var date = this.dates[i];
            if( i == 0 || (i + 1) % 8 === 0){
                // new line
                line = this.rowTemplate.toDOM();
                this.DOMElements.daysContainer.appendChild(line);

            }
            var cell = this.cellTemplate.toDOM();
            cell.setAttribute('date',date.date);
            cell.setAttribute('month',date.month);
            cell.setAttribute('year',date.year);
            /* check classes */
            // today & active line
            if(today.getDate() == date.date && today.getMonth() == date.month && today.getFullYear() == date.year){
                cell.classList.add(this.todayCellClass);
                line.classList.add(this.activeLineClass);
            }
            //active date
            if(curDate == date.date && curMonth == date.month && curYear == date.year){
                cell.classList.add(this.activeCellClass);
            }
            //another month
            if(curFocusMonth != date.month){
                cell.classList.add(this.anotherMonthClass);
            }
            var textContainer = cell.getElementsByClassName('date_cell')[0];
            textContainer.innerText = date.date;
            var self = this;
            cell.addEventListener('click', function(){
                var date = this.getAttribute('date');
                var month = this.getAttribute('month');
                var year = this.getAttribute('year');
                var newDate = new Date(year, month, date);
                self.setDate(newDate);
            });
            line.appendChild(cell);
        }
        this.drawMonthLine();
    }

    this.drawMonthLine = function(){
        this.DOMElements.monthContainer.innerHTML = this.monthLabel(this.focus.getMonth()) + ', ' + this.focus.getFullYear();
    }

    // init
    this.init();
}
Date.prototype.getDaysInMonth = function(){
    return (new Date(this.getFullYear(), this.getMonth() + 1, 0)).getDate();
};
