Element.prototype.initSlider = function(id, onDrag) {
    var range = this,
        dragger = range.children[0],
        draggerWidth = 0, // width of your dragger
        down = false,
        rangeWidth, rangeLeft;

    dragger.style.width = draggerWidth + 'px';
    dragger.addEventListener("mousedown", function(){
        return false;
    })
    range.addEventListener("mousedown", function(e) {
        var rect = this.getBoundingClientRect();
        rangeWidth = rect.width;
        rangeLeft = rect.left;
        down = true;
        updateDragger(e);
        return false;
    });

    document.addEventListener("mousemove", function(e) {
        updateDragger(e);
    });

    document.addEventListener("mouseup", function() {
        down = false;
    });

    function updateDragger(e) {
        if (down) {
            var width = e.pageX - rangeLeft;
            width = width < 0 ? 0 : width;
            width = width >rangeWidth ? rangeWidth : width;
            dragger.style.width =  width + 'px';
            var percent = Math.round(((e.pageX - rangeLeft) / rangeWidth) * 100);
            percent = percent > 100 ? 100 : percent;
            percent = percent < 0 ? 0 : percent;
            if (typeof onDrag == "function") onDrag(percent);
        }
    }

}