function ScheduleVisualizer(shedule){
    this.timeMarkerContainerHTML = '<div class="time_marker_container"></div>';
    this.timeMarkerContainer = undefined;
    this.timeMarkerHTML = '<div class="time_marker"></div>';
    this.textMarkerHTML = '<div class="text"></div>';
    this.periodsContainerHTML = '<div class="periods_container"></div>';
    this.periodsContainer = undefined;
    this.periodBlockHTML = '<div class="period_indicator"></div>';
    this.offsetWidth = 0;
    this.minTime = 0;
    this.maxTime = 1440;
    this.textMarkerOffset = 0;
    this.textMarkerStep = 6;
    this.schedule = shedule || new Schedule();
    this.schedule.subscribe(this);
    this.vizualizerDOM = undefined;
    this.vizualizeTo = function(vizualizerDOM){
        this.schedule.vizualizer = vizualizerDOM;
        if(!vizualizerDOM){throw new Error('DOM Vizualizer required')}
        this.vizualizerDOM = vizualizerDOM;
        this.calculateOffsetWidth();
        this.drawTimeMarkers();
        this.drawPeriods();
    };

    this.drawTimeMarkers = function(){
        if(this.timeMarkerContainer){
            this.timeMarkerContainer.outerHTML = '';
        }else{
            this.timeMarkerContainer = this.timeMarkerContainerHTML.toDOM();
        }
        this.vizualizerDOM.appendChild(this.timeMarkerContainer);
        for (var i = this.minTime; i <= this.maxTime / 60; i++){
            var new_marker = this.timeMarkerHTML.toDOM();
            new_marker.style.left = i * this.offsetWidth * 6 + this.offsetWidth * 6 + '%';
            if((i - this.textMarkerOffset) % this.textMarkerStep === 0){
                var new_text_marker = this.textMarkerHTML.toDOM();
                new_text_marker.innerHTML = pad(i,2) + '.00';
                new_marker.appendChild(new_text_marker);
            }
            this.timeMarkerContainer.appendChild(new_marker);
        }
    };

    this.drawPeriods = function(){
        if(this.schedule.fixMode){
            this.drawMoments();
            return;
        }
        if(this.periodsContainer){
            this.periodsContainer.innerHTML = '';
        }else{
            this.periodsContainer = this.periodsContainerHTML.toDOM();
        }
        this.vizualizerDOM.appendChild(this.periodsContainer);
        for (var i = 0; i < this.schedule.periods.length; i++){
            var periodObj = this.schedule.periods[i];
            var new_period_block = this.periodBlockHTML.toDOM();
            new_period_block.style.left = periodObj.timeFrom / 10 * this.offsetWidth + this.offsetWidth * 6 + '%';
            new_period_block.style.width = (periodObj.timeTill - periodObj.timeFrom) / 10 * this.offsetWidth + '%';
            new_period_block.style.backgroundColor = periodObj.getColor();
            this.periodsContainer.appendChild(new_period_block);
        }
    };
    this.drawMoments = function(){
        if(this.periodsContainer){
            this.periodsContainer.innerHTML = '';
        }else{
            this.periodsContainer = this.periodsContainerHTML.toDOM();
        }
        this.vizualizerDOM.appendChild(this.periodsContainer);
        for (var i = 0; i < this.schedule.periods.length; i++){
            var periodObj = this.schedule.periods[i];
            var new_period_block = this.periodBlockHTML.toDOM();
            new_period_block.style.left = (periodObj.timeTill) / 10 * this.offsetWidth + this.offsetWidth * 5.3 + '%';
            new_period_block.style.width = this.offsetWidth * 2 + '%';
            new_period_block.style.backgroundColor = periodObj.getColor();
            this.periodsContainer.appendChild(new_period_block);
        }
    }

    this.calculateOffsetWidth = function(){
        var parts = (this.maxTime - this.minTime)/10 + 2*6;
        this.offsetWidth = 100/parts;
    };

    this.scheduleChanged = function(){
        this.drawPeriods();
    }
}

function Schedule(args){
    args = args||{};
    this.periodTemplateID = args.periodTemplateID || 'periodTemplate';
    this.periodsContainerID = args.periodsContainerID || 'periodLineContainer';
    this.fixMode = args.fixMode || false;
    this.periods = [];
    this.maxPeriods = args.maxPeriods || 4;
    this.minPeriodLength = 60;
    this.defaultFrequency = 60;
    this.maxTime = 1440;
    this.vizualizer = undefined;
    this.prototype = Element.prototype;
    this.subscribers = [];
    this.maxReachedEvent = document.createEvent('Event');
    this.maxReachedEvent.initEvent('maxReached', true, true);
    this.maxNotReachedEvent = document.createEvent('Event');
    this.maxNotReachedEvent.initEvent('maxNotReached', true, true);
    this.addPeriod = function(periodData){
        if(!periodData && !this.fixMode){
            periodData = {};
            var rightPeriodObj = this.findEmptyRightPeriod();
            if(!rightPeriodObj){
                alert('There is no place to insert new period! The minimal period duration is ' + this.minPeriodLength + ' minutes.');
                return false;
            }
            periodData.timeFrom = rightPeriodObj.timeFrom;
            periodData.timeTill = rightPeriodObj.timeTill;
            periodData.frequency = this.defaultFrequency;
        }
        if(this.fixMode && !periodData){
            periodData = {};
            periodData.timeFrom = 0;
            periodData.timeTill = this.findNextHourMoment();
            periodData.frequency = 10;
        }
        if(this.periods.length >= this.maxPeriods - 1 ){
            this.vizualizer.dispatchEvent(this.maxReachedEvent);
        }
        if(this.periods.length >= this.maxPeriods ){
            throw new Error('Max period quantity reached.');
        }
        periodData.schedule = this;
        periodData.minLength = this.minPeriodLength;
        periodData.momentMode = this.fixMode;
        var new_period = new Period(periodData);
        this.periods.push(new_period);
        var self = this;
        var periods_vizualizer = new PeriodVizualizer(new_period, this.periodTemplateID);
        new_period.setVizualizer(periods_vizualizer);
        new_period.DOMView.addEventListener('change', function(){
            self.onChange();
        });
        var period_line = document.querySelector('[scheduleid="' + this.periodsContainerID + '"]');
        periods_vizualizer.vizualizeTo(period_line);
        this.onChange();
        return this;
    };
    this.findEmptyRightPeriod = function(){
        var emptyPeriods = this.emptyPeriods();
        if(emptyPeriods.length > 0){
            return emptyPeriods[emptyPeriods.length - 1];
        }else{
            return false;
        }
    }
    this.findNextHourMoment = function(){
        var timeTill = this.periods[this.periods.length-1].timeTill;
        return timeTill + 60 > this.maxTime ? 60 : timeTill + 60;
    }
    this.findEmptyLeftPeriod = function(){
        var emptyPeriods = this.emptyPeriods();
        if(emptyPeriods.length > 0){
            return emptyPeriods[0];
        }else{
            return false;
        }
    }
    this.emptyPeriods = function(){
        var statePoints = [];
        statePoints.push(0);
        for(var i = 0; i < this.periods.length; i++){
            statePoints.push(this.periods[i].timeFrom);
            statePoints.push(this.periods[i].timeTill);
        }
        statePoints.push(this.maxTime);
        statePoints = statePoints.sort(function (a,b) {
            return a-b;
        });
        var result = [];
        var isEmpty = true;
        var lastStatePoint = 0;
        for(var i = 1; i < statePoints.length; i++){
            var statePoint = statePoints[i];
            if(statePoint - lastStatePoint >= this.minPeriodLength && isEmpty){
                result.push({
                    timeFrom : lastStatePoint,
                    timeTill : statePoint
                })
            }
            lastStatePoint = statePoint;
            isEmpty = !isEmpty;
        }
        return result;
    }
    this.getPeriodID = function(period){
        for(var i = 0; i < this.periods.length; i++){
            if(this.periods[i] == period) return i;
        }
    };
    this.removePeriod = function(period){
        var periodID = this.getPeriodID(period);
        this.periods.splice(periodID,1);
        if(this.periods.length < this.maxPeriods) {
            this.vizualizer.dispatchEvent(this.maxNotReachedEvent);
        }
        this.onChange();
    };
    this.import = function(serialized_string){
        this.eraseMe();
        var importArr = JSON.parse(serialized_string);
        for(var i = 0; i < importArr.length; i++){
            var periodData = {
                timeFrom : importArr[i].timeFrom,
                timeTill : importArr[i].timeTill,
                frequency : importArr[i].frequency,
                schedule : this
            }
        }
        this.onChange();
    };
    this.eraseMe = function(){
        this.periods = [];
    };
    this.export = function(){
        var exportArr = [];
        for(var i = 0; i < this.periods.length; i++){
            if(this.fixMode){
                exportArr.push(this.periods[i].timeTill);
            }else{
                exportArr.push({
                    timeFrom : this.periods[i].timeFrom,
                    timeTill : this.periods[i].timeTill,
                    frequency : this.periods[i].frequency,
                });
            }
        }
        return JSON.stringify(exportArr);
    };
    this.onChange = function(){
        this.calcPeriodColors();
        if(!this.fixMode) this.calcPeriodsRanges();
        this.notifySubscribers();
    };
    this.calcPeriodColors = function(){
        for (var i = 0; i < this.periods.length; i++){
            var period = this.periods[i];
            period.DOMStruct.periodRange.setColor(period.getColor());
        }
    }
    this.calcPeriodsRanges = function(){
        for (var i = 0; i < this.periods.length; i++){
            var timeFrom = this.periods[i].timeFrom;
            var timeTill = this.periods[i].timeTill;
            this.periods[i].setRange({
                left : this.findLeftBorder(timeFrom, i),
                right : this.findRightBorder(timeTill, i)
            });
        }
    }
    this.findLeftBorder = function(point, periodIndex){
        var rightBorders = [];
        for(var i = 0; i < this.periods.length; i++){
            if(this.periods[i].timeTill <= point && i != periodIndex){
                rightBorders.push(this.periods[i].timeTill)
            }
        }
        return rightBorders.length > 0 ? getMaxOfArray(rightBorders) : 0;
    }
    this.findRightBorder = function(point, periodIndex){
        var leftBorders = [];
        for(var i = 0; i < this.periods.length; i++){
            if(this.periods[i].timeFrom >= point && i != periodIndex){
                leftBorders.push(this.periods[i].timeFrom)
            }
        }
        return leftBorders.length > 0 ? getMinOfArray(leftBorders) : this.maxTime;
    }
    this.notifySubscribers = function(){
        for(var i = 0; i < this.subscribers.length; i++){
            this.subscribers[i].scheduleChanged();
        }
    };

    this.subscribe = function(obj){
        this.subscribers.push(obj);
    };
    this.unsubscribe = function(obj){
        for(var i = 0; i < this.subscribers.length; i++){
            if(this.subscribers[i] == obj){
                delete this.subscribers[i];
                return true;
            }
        }
        return false;
    }
}

function Period(args){
    /* default values */
    this.defaultColor = 'fff';
    this.defaultTimeFrom = 0;
    this.defaulttimeTill = 1;
    this.defaultFrequency = 60;
    this.subscribers = [];
    this.colorsList = [
        'FF0D93',
        '54bcb6',
        'FF4000',
        'F9A400',
        'FF0D93',
        '54bcb6',
        'FF4000',
        'F9A400',
        'FF0D93',
        '54bcb6',
    ];
    /* check insert data */
    args = args || {};
    this.timeFrom = args.timeFrom || this.defaultTimeFrom;
    this.timeTill = args.timeTill || this.defaulttimeTill;
    this.minLength = args.minLength || 1;
    this.frequency = args.frequency || this.defaultFrequency;
    this.schedule = args.schedule || {};
    this.momentMode = args.momentMode || false;
    this.color = args.color || this.defaultColor;
    this.vizualizer = undefined;
    this.DOMStruct = {};
    this.setRange = function(rangeObj){
        if(this.DOMStruct.periodRange){
            this.DOMStruct.periodRange.setRange(rangeObj);
        }
    }
    this.getColor = function(){
        var color;
        if(this.colorsList[this.getMyIndex()]){
            color = this.colorsList[this.getMyIndex()]
        }else{
            color = this.defaultColor;
        }
        return '#' + color;
    }
    this.getMyIndex = function(){
        return this.schedule.getPeriodID(this);
    }
    this.subscribe = function(obj){
        this.subscribers.push(obj);
    }

    this.onChange = function(){
        this.notifySubscribers();
    }
    this.notifySubscribers = function(){
        for(var i = 0; i < this.subscribers.length; i++){
            this.subscribers[i].periodsChanged();
        }
    }
    this.changeEvent = document.createEvent('Event');
    this.changeEvent.initEvent('change', true, true);
    this.set = function(propertyName, value){
        switch(propertyName){
            case 'timeFrom' :
                this.setTimeFrom(value);
                break;
            case 'timeTill' :
                this.setTimeTill(value);
                break;
            case 'frequency' :
                this.setFrequency(value);
                break;
        }
        this.DOMView.dispatchEvent(this.changeEvent);
    }
    this.setTimeFrom = function(min){
        this['timeFrom'] = min;
    }
    this.setTimeTill = function(min){
        this['timeTill'] = min;
    }
    this.setFrequency = function(min){
        this['frequency'] = min;
    }
    this.remove = function(){
        this.schedule.removePeriod(this);
        this.DOMView.outerHTML = '';
        if(this.schedule.periods.length == 0){
            this.vizualizer.vizualizerContainer.classList.add('is_empty');
        }
        delete this.vizualizer;
        delete this;
    }
    this.setVizualizer = function(vizualizer){
        this.vizualizer = vizualizer;
    }
}

function PeriodVizualizer(period, templateID) {
    this.period = period || new Period();
    this.period.setVizualizer(this);
    this.layoutData = [
        'frequencyOutput',
        'frequencyInput',
        'periodFromOutput',
        'periodFromInput',
        'periodTillOutput',
        'periodTillInput',
        'deleteButton',
        'frequencyRange',
        'periodRange'
    ];
    if(!templateID){
        throw new Error('Template URL required!');
        return false;
    }
    this.vizualizerContainer = undefined;
    this.template = '';

    this.vizualizeTo = function(vizualizerContainer){
        if(!vizualizerContainer){
            throw new Error('DOM Vizualizer required');
            return false;
        }
        this.vizualizerContainer = vizualizerContainer;
        this.draw();
    }

    this.loadTemplate = function(){
        var templateObj = document.getElementById(templateID);
        templateObj.style.display = 'block';
        this.template = templateObj.outerHTML;
        templateObj.style.display = 'none';
    }
    this.loadTemplate();
    this.period.DOMView = this.template.toDOM();
    this.period.DOMView.id = '';

    this.draw = function(){
        this.vizualizerContainer.appendChild(this.period.DOMView);
        this.vizualizerContainer.classList.remove('is_empty');
        var dom = this.period.DOMStruct;
        for(var i = 0; i < this.layoutData.length; i++){
            var className = this.layoutData[i];
            dom[className] = this.period.DOMView.getElementsByClassName(className)[0];
        }
        var self = this;
        /* BEGIN create ranges */
        if(dom.frequencyRange){
            var frequencyScale = {5 : '5m',10 : '10m',20 : '20m',30 : '30m',60 : '1h',120 : '2h',180 : '3h',240 : '4h',300 : '5h',};
            dom.frequencyRange.initMinMaxRange({
                scale : frequencyScale,
                max : this.period.frequency,
                fixMin : true,
                showGrid : true,
                showScaleText : true
            });
            dom.frequencyRange.addEventListener('change',function () {
                dom.frequencyInput.value = this.max;
                dom.frequencyInput.onchange();
            });

            dom.frequencyInput.onchange = function(){
                self.period.set('frequency',parseInt(this.value));
            };
        }

        if(dom.periodRange){
            var hoursScale = {};
            for(var i = 0; i <= 24 * 60; i += 10){
                hoursScale[i] = i;
            }
            dom.periodRange.initMinMaxRange({
                scale : hoursScale,
                min : this.period.timeFrom,
                max : this.period.timeTill,
                minLength : this.period.minLength / 10,
                color : this.period.getColor(),
                fixMin : this.period.momentMode
            });
            dom.periodRange.addEventListener('change',function () {
                dom.periodFromInput.value = this.min;
                dom.periodFromInput.onchange();
                dom.periodTillInput.value = this.max;
                dom.periodTillInput.onchange();
            });
        }


        /* END create ranges */
        this.period.DOMView.addEventListener('change',function () {
            dom.frequencyOutput.innerHTML = self.Output.formatFreq(self.period.frequency);
            dom.periodFromOutput.innerHTML = self.Output.formatTime(self.period.timeFrom);
            dom.periodTillOutput.innerHTML = self.Output.formatTime(self.period.timeTill);
        })
        dom.periodFromInput.onchange = function(){
            self.period.set('timeFrom',parseInt(this.value));
        };
        dom.periodTillInput.onchange = function(){
            self.period.set('timeTill',parseInt(this.value));
        };
        dom.deleteButton.onclick = function(){
            self.period.remove();
        }

        dom.frequencyInput.value = this.period.frequency;
        dom.periodFromInput.value = this.period.timeFrom;
        dom.periodTillInput.value = this.period.timeTill;

        this.period.DOMView.dispatchEvent(this.period.changeEvent)
    }

    this.Output = {
        formatFreq : function(minutes){
            minutes = parseInt(minutes);
            return minutes >= 60 ? parseInt(minutes/60) + ' hours' : minutes + ' minutes'
        },
        formatTime : function(minutes){
            minutes = parseInt(minutes);
            var hoursPart = Math.floor(minutes / 60);
            var minsPart = pad(minutes - hoursPart * 60, 2);
            return hoursPart > 12 ? pad(hoursPart - 12, 2) + ':'+minsPart+' pm' : pad(hoursPart,2) + ':'+minsPart+' am'
        }
    }
    this.periodsChanged = function(){

    }
}
